<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CardController;
use App\Http\Controllers\ColumnController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});

//Auth::routes();
//
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', function () {
    return redirect()->route('cards.index');
})->name('home');


Route::get('cards', [CardController::class, 'index'])->name('cards.index');
Route::post('cards', [CardController::class, 'store'])->name('cards.store');
Route::put('cards/sync', [CardController::class, 'sync'])->name('cards.sync');
Route::put('cards/{card}', [CardController::class, 'update'])->name('cards.update');
Route::delete('columns/{column}', [ColumnController::class, 'destroy'])->name('columns.destroy');
Route::post('columns', [ColumnController::class, 'store'])->name('columns.store');
Route::put('columns', [ColumnController::class, 'update'])->name('columns.update');


//Auth::routes();
//
//Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

