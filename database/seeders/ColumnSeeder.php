<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ColumnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('columns')->delete();

        \DB::table('columns')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'title' => 'Backlog',
                    'slug' => 'backlog',
                    'order' => 1,
                ),
            1 =>
                array (
                    'id' => 2,
                    'title' => 'Up Next',
                    'slug' => 'up-next',
                    'order' => 2,
                ),
            2 =>
                array (
                    'id' => 3,
                    'title' => 'In Progress',
                    'slug' => 'in-progress',
                    'order' => 3,
                ),
            3 =>
                array (
                    'id' => 4,
                    'title' => 'Done',
                    'slug' => 'done',
                    'order' => 4,
                ),
        ));

    }
}
