<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
//    use HasFactory;
    protected $fillable = ['title', 'slug', 'order'];

    public $timestamps = false;

    public function cards()
    {
        return $this->hasMany(Card::class)->orderBy('order');
    }


}
