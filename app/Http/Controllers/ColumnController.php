<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\Column;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ColumnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string', 'max:56'],
        ]);

        $slug = str_replace(" ", "-", $request->title);
        $request->request->add(['slug' => $slug]);
        $order = DB::table('columns')->max('order');
        $order = $order + 1;
        $request->request->add(['order' => $order]);
        Column::create($request->only('title', 'slug', 'order'));
        $cards = Column::with('cards')->get();
        return $cards;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Column $column
     * @return \Illuminate\Http\Response
     */
    public function show(Column $column)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Column $column
     * @return \Illuminate\Http\Response
     */
    public function edit(Column $column)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Column $column
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Column $column)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Column $column
     * @return \Illuminate\Http\Response
     */
    public function destroy(Column $column)
    {
        if ($column->delete()) {
            $cards = Column::with('cards')->get();
            return response()->json(['error' => false, 'cards' => $cards]);
        } else {
            return response()->json(['error' => true, 'msj' => 'Column can´t be deleted.']);
        }

    }
}
